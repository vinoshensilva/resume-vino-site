// Import material ui
import styled from "@mui/system/styled";

// Import components
import Sidebar from "@/components/Layout/Sidebar";
import Hero from "@/components/Layout/Hero";
import About from "@/components/Layout/About";
import Footer from "@/components/Layout/Footer";
import Contact from "@/components/Layout/Contact";
import Experiences from "@/components/Layout/Experiences";
import Portfolios from "@/components/Layout/Portfolios";
import Portfolio from "@/components/Layout/Portfolio";
import Technologies from "@/components/Layout/Technologies";

import ScrollTop from "@/components/UI/ScrollTop";
import AutoScroll from "@/components/HOC/AutoScroll";

const StyledBody = styled("div")(
  ({ theme }) => `
  display: flex;
  flex-direction: row;

  @media (max-width: ${theme.breakpoints.values.md}px){
        flex-direction: column;
  }
`
);

const StyledMain = styled("main")`
  width: 100%;
  height: 100%;
  flex-grow: 1;
`;

function App() {
  return (
    <StyledBody>
      <Sidebar />
      <StyledMain>
        <AutoScroll>
          <Hero />
          <About />
          <Portfolios />
          <Portfolio />
          <Technologies />
          <Experiences />
          <Contact />
          <Footer />
          <ScrollTop />
        </AutoScroll>
      </StyledMain>
    </StyledBody>
  );
}

export default App;
