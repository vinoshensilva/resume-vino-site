// Import reducers
import portfolioReducer from "@/reducer/portfolio";

const reducer = {
  portfolios: portfolioReducer,
};

export default reducer;
