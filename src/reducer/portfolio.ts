// Import libraries
import { createSlice, PayloadAction } from "@reduxjs/toolkit";

// Import types
import PortFolioType from "@/types/portfolio";

// Import data
import PORTFOLIOS, {
  ALL_PROJECTS,
  PORTFOLIO_CATEGORIES,
} from "@/data/portfolios";
import { TStore } from "@/store";

// Define state interface
export interface portfolioState {
  portfolios: PortFolioType[];
  categories: string[];
  category: string;
  portfolioId: string;
}

// Define payload types
type TShowProject = {
  portfolioId: string;
};

type TChangeCategory = {
  newCategory: string;
};

const initialState: portfolioState = {
  portfolios: PORTFOLIOS,
  categories: PORTFOLIO_CATEGORIES,
  portfolioId: "",
  category: ALL_PROJECTS,
};

const portfolioSlice = createSlice({
  name: "portfolio",
  initialState,
  reducers: {
    hideProject: (state: portfolioState) => {
      state.portfolioId = "";
    },
    showProject: (
      state: portfolioState,
      { payload }: PayloadAction<TShowProject>
    ) => {
      const { portfolioId } = payload;

      state.portfolioId = portfolioId;
    },
    changeCategory: (
      state: portfolioState,
      { payload }: PayloadAction<TChangeCategory>
    ) => {
      const { newCategory } = payload;

      state.category = newCategory;
    },
  },
});

export const getCategory = (state: TStore) => state.portfolios.category;

export const selectAllCategories = (state: TStore) =>
  state.portfolios.categories;

// export const selectAllPortfolio = (state: TStore) => state.portfolios.portfolios;

export const getSelectedPortfolio = (state: TStore) => {
  return state.portfolios.portfolios.find(
    (portfolio) => portfolio.id === state.portfolios.portfolioId
  );
};

export const selectPortfoliosByCategory = (state: TStore) => {
  return state.portfolios.portfolios.filter((portfolio) => {
    return (
      state.portfolios.category === ALL_PROJECTS ||
      portfolio.category.includes(state.portfolios.category)
    );
  });
};

export default portfolioSlice.reducer;

export const { hideProject, showProject, changeCategory } =
  portfolioSlice.actions;
