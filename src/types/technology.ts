type TechnologyType = {
  image: string;
  name: string;
};

export default TechnologyType;
