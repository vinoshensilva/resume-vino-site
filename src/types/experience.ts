type ExperienceType = {
  image: string;
  description: string;
  company: string;
  position: string;
  tech: string;
  timeline: string;
  id: string;
};

export default ExperienceType;
