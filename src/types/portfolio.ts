type PortFolioType = {
  company: string;
  year: string;
  name: string;
  description: string;
  thumbnail: string;
  images: string[];
  category: string[];
  link: string | undefined;
  id: string;
  result: string;
  note: string | undefined;
};

export default PortFolioType;
