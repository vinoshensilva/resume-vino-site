import TechnologyType from "@/types/technology";

const technologies: TechnologyType[] = [
  {
    image: "/images/techs/tech-fauna.png",
    name: "FaunaDB",
  },
  {
    image: "/images/techs/tech-javascript.png",
    name: "JavaScript",
  },
  {
    image: "/images/techs/tech-material-ui.png",
    name: "Material UI",
  },
  {
    image: "/images/techs/tech-netlify.png",
    name: "Netlify",
  },
  {
    image: "/images/techs/tech-next-js.png",
    name: "Next.js",
  },
  {
    image: "/images/techs/tech-node-js.png",
    name: "Node.js",
  },
  {
    image: "/images/techs/tech-unity.png",
    name: "Unity",
  },
  {
    image: "/images/techs/tech-react.png",
    name: "React",
  },
  {
    image: "/images/techs/tech-redux.png",
    name: "Redux",
  },
  {
    image: "/images/techs/tech-react.png",
    name: "React Native",
  },
  {
    image: "/images/techs/tech-tailwind.png",
    name: "Tailwind",
  },
  {
    image: "/images/techs/tech-typescript.png",
    name: "TypeScript",
  },
  {
    image: "/images/techs/tech-expo.png",
    name: "Expo",
  },
];

export default technologies;
