import { createTheme, Theme } from "@mui/material";

const theme: Theme = createTheme({
  palette: {
    primary: {
      main: "#EA2845",
    },
    secondary: {
      main: "#111827",
    },
    background: {
      paper: "#111827",
      default: "#FFFFFF",
    },
    text: {
      primary: "#EA2845",
      secondary: "#111827",
    },
  },
  typography: {
    fontFamily: ["Poppins", "Helvetica", "Arial", "sans-serif"].join(","),
  },
  components: {
    MuiButtonBase: {
      defaultProps: {
        disableRipple: true,
      },
    },
    MuiCssBaseline: {
      styleOverrides: {
        body: {
          scrollbarColor: "#ea2845 #FFFFFF",
          "&::-webkit-scrollbar, & *::-webkit-scrollbar": {
            backgroundColor: "#FFFFFF",
            width: "12px",
          },
          "&::-webkit-scrollbar-thumb, & *::-webkit-scrollbar-thumb": {
            borderRadius: 8,
            backgroundColor: "#ea2845",
            // minHeight: 24,
            border: "1px solid #ea2845",
            width: "12px",
          },
          "&::-webkit-scrollbar-thumb:focus, & *::-webkit-scrollbar-thumb:focus":
          {
            backgroundColor: "#ea2845",
          },
          "&::-webkit-scrollbar-thumb:active, & *::-webkit-scrollbar-thumb:active":
          {
            backgroundColor: "#ea2845",
          },
          "&::-webkit-scrollbar-thumb:hover, & *::-webkit-scrollbar-thumb:hover":
          {
            backgroundColor: "#ea2845",
          },
          "&::-webkit-scrollbar-corner, & *::-webkit-scrollbar-corner": {
            backgroundColor: "#111827",
          },
        },
      },
    },
  },
});

export default theme;
