import { useEffect } from "react";

const useScroll = (onScroll: EventListener) => {
  useEffect(() => {
    window.addEventListener("scroll", onScroll);
    return () => {
      window.removeEventListener("scroll", onScroll);
    };
  });
};

export default useScroll;
