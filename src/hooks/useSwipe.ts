// Import libraries
import { useEffect } from "react";

export enum SwipeDirection {
  None = 0,
  Up,
  Down,
  Left,
  Right
}

const useSwipe = (
  element: HTMLElement | null,
  onSwipe: (direction: SwipeDirection) => void
) => {
  let touchStartX = 0;
  let touchStartY = 0;
  let touchEndX = 0;
  let touchEndY = 0;

  const onSwipeStart = (event: TouchEvent) => {
    touchStartX = event.changedTouches[0].pageX;
    touchStartY = event.changedTouches[0].pageY;

  }

  const onSwipeEnd = (event: TouchEvent) => {
    touchEndX = event.changedTouches[0].pageX;
    touchEndY = event.changedTouches[0].pageY;

    handleSwipe();
  }

  const onMouseDown = (event: MouseEvent) => {
    touchStartX = event.pageX;
    touchStartY = event.pageY;
  }

  const onMouseUp = (event: MouseEvent) => {
    touchEndX = event.pageX;
    touchEndY = event.pageY;

    handleSwipe();
  }

  const handleSwipe = () => {
    if (touchEndX < touchStartX) {
      onSwipe(SwipeDirection.Left);
    }
    else if (touchEndX > touchStartX) {
      onSwipe(SwipeDirection.Right);
    }
    else if (touchEndY < touchStartY) {
      onSwipe(SwipeDirection.Down);
    }
    else if (touchEndY > touchStartY) {
      onSwipe(SwipeDirection.Up);
    }

    onSwipe(SwipeDirection.None);
  }

  useEffect(() => {
    element?.addEventListener("touchstart", onSwipeStart, { passive: true });
    element?.addEventListener("touchend", onSwipeEnd, { passive: true });
    element?.addEventListener("mousedown", onMouseDown, { passive: true });
    element?.addEventListener("mouseup", onMouseUp, { passive: true });

    return () => {
      element?.removeEventListener("touchstart", onSwipeStart);
      element?.removeEventListener("touchend", onSwipeEnd);
      element?.removeEventListener("mousedown", onMouseDown);
      element?.removeEventListener("mouseup", onMouseUp);
    };
  }, [element]);
};

export default useSwipe;