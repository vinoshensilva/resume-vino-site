// Import material ui
import Typography from "@mui/material/Typography";
import styled from "@mui/system/styled";

const StyledFooter = styled('footer')(({ theme }) => `
    padding: 2em 2em;
    background-color:${theme.palette.secondary.main};
`);

const Footer = () => {
    return (
        <StyledFooter>
            <Typography title="@ Copyright 2022 - Vinoshen Silva" variant="body1">
                @ Copyright 2022 - Vinoshen Silva
            </Typography>
        </StyledFooter>
    )
}

export default Footer;