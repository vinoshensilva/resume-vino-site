// Import material ui
import styled from "@mui/system/styled";
import Box from "@mui/material/Box";
import { withTheme } from "@emotion/react";
import { grey } from "@mui/material/colors";
import Typography from "@mui/material/Typography";
import Link from "@mui/material/Link";
import Avatar from "@mui/material/Avatar";
import { ButtonBase } from "@mui/material";
import { useState } from "react";

const PlaceHolderbar = styled(Box)`
  width: 240px;
  height: 100%;
  flex-shrink: 0;

  @media (max-width: ${(props) => props.theme.breakpoints.values.md}px) {
    display: none;
  }
`;

const StyledSidebar = withTheme(styled(Box)`
  width: 240px;
  flex-grow: 0;
  background: ${grey[100]};
  padding: 2em;
  text-align: center;
  position: fixed;
  height: 100%;

  flex-direction: column;
  gap: 0.5em;

  @media (max-width: ${(props) => props.theme.breakpoints.values.md}px) {
    background: white;
    width: 100%;
    padding: 1em 2em 1em 2em;
    text-align: left;
    position: relative;
  }
`);

const StyledFlexBox = styled(Box)`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  height: 100%;

  @media (max-width: ${(props) => props.theme.breakpoints.values.md}px) {
    flex-direction: row;
  }
`;

const StyledProfile = withTheme(styled(Box)`
  display: flex;
  flex-direction: column;
  gap: 1em;

  @media (max-width: ${(props) => props.theme.breakpoints.values.md}px) {
    #profile {
      display: none;
    }

    display: block;
  }
`);

const StyledSocialBox = styled(Box)`
  display: flex;
  flex-direction: row;
  gap: 1em;
  justify-content: space-between;
`;

const StyledNavbar = styled("nav")`
  ul {
    display: flex;
    flex-direction: column;
    gap: 1em;

    text-align: center;
    list-style-type: none;
    padding: 0;
    margin: 0;
  }

  ul li {
    color: ${(props) => props.theme.palette.secondary.main};
  }

  @media (max-width: ${(props) => props.theme.breakpoints.values.md}px) {
    display: none;
  }
`;

interface StyledMobileNavbarProps {
  show: boolean;
}

const StyledMobileNavbar = styled("nav")<StyledMobileNavbarProps>`
  transition: max-height 0.25s ease-in;

  overflow: hidden;

  ul {
    display: flex;
    flex-direction: column;
    gap: 1em;

    text-align: center;
    list-style-type: none;
    padding: 1.5em 0em 1em 0em;
    margin: 0;
  }

  ul li {
    color: ${(props) => props.theme.palette.secondary.main};
    border-bottom: 1px solid ${grey[200]};
  }

  @media (max-width: ${(props) => props.theme.breakpoints.values.md}px) {
    ul {
      text-align: left;
    }

    ${(props) => {
      if (props.show) {
        return `
                            max-height: 260px; 
                    `;
      } else {
        return `
            max-height: 0;
                    `;
      }
    }}
  }
`;

const StyledLink = withTheme(styled(Link)`
  color: ${(props) => props.theme.palette.secondary.main};

  &:hover {
    color: ${(props) => props.theme.palette.primary.main};
  }

  &.active {
    color: ${(props) => props.theme.palette.primary.main};
  }
`);

const StyledHamburgerIcon = styled(ButtonBase)(
  ({ theme }) => `
    width: 24px;
    height: 24px;
    display: none;

    &:hover {
        color: ${theme.palette.secondary.main}
    }

    @media (max-width: ${theme.breakpoints.values.md}px){
        display: block;
    }

`
);

const UnorderedListLinks = () => (
  <ul>
    <li>
      <StyledLink href="#" underline="none">
        Home
      </StyledLink>
    </li>
    <li>
      <StyledLink href="#about" underline="none">
        About
      </StyledLink>
    </li>
    <li>
      <StyledLink href="#portfolios" underline="none">
        Portfolios
      </StyledLink>
    </li>
    <li>
      <StyledLink href="#technologies" underline="none">
        Technologies
      </StyledLink>
    </li>
    <li>
      <StyledLink href="#experiences" underline="none">
        Experiences
      </StyledLink>
    </li>
    <li>
      <StyledLink href="#contact" underline="none">
        Contact
      </StyledLink>
    </li>
  </ul>
);

const Sidebar = () => {
  const [show, setShow] = useState(false);

  const onClickShowNav = () => {
    setShow((prevState: boolean) => !prevState);
  };

  return (
    <>
      <PlaceHolderbar />
      <StyledSidebar>
        <StyledFlexBox>
          <StyledProfile>
            <Avatar
              sx={{
                height: "auto",
                width: "100%",
              }}
              id="profile"
              alt="Vinoshen Profile Picture"
              src="/images/profile.webp"
            />
            <Typography fontSize="26px" fontWeight="700" variant="h1">
              Vinoshen
            </Typography>
            <StyledNavbar>
              <UnorderedListLinks />
            </StyledNavbar>
          </StyledProfile>
          <StyledSocialBox>
            <a
              href="https://www.linkedin.com/in/vino-silva-b15a0b20b"
              target="_blank"
            >
              <img
                title="LinkedIn"
                width="24px"
                height="24px"
                src="/images/icons/linkedin.png"
              />
            </a>
            <a href="https://gitlab.com/vinoshensilva" target="_blank">
              <img
                title="GitLab"
                width="24px"
                height="24px"
                src="/images/icons/gitlab.png"
              />
            </a>
            <a
              href="/pdf/vino-resume.pdf"
              target="_blank"
              rel="noreferrer"
              style={{ textDecoration: "none", marginTop: "-3px" }}
            >
              <img
                title="GitLab"
                width="29px"
                height="29px"
                src="/images/icons/pdf.png"
              />
            </a>
            <a
              href="https://www.freecodecamp.org/VinoshenSilva"
              target="_blank"
            >
              <img
                title="FreeCodeCamp"
                width="24px"
                height="24px"
                src="/images/icons/freecodecamp.png"
              />
            </a>
            <StyledHamburgerIcon onClick={onClickShowNav}>
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="24px"
                height="24px"
                fill="none"
                viewBox="0 0 24 24"
                stroke="currentColor"
                strokeWidth="2"
              >
                <path strokeLinejoin="round" d="M4 6h16M4 12h16M4 18h16" />
              </svg>
            </StyledHamburgerIcon>
          </StyledSocialBox>
        </StyledFlexBox>
        <StyledMobileNavbar show={show}>
          <UnorderedListLinks />
        </StyledMobileNavbar>
      </StyledSidebar>
    </>
  );
};

export default Sidebar;
