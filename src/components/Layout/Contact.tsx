import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';
import Link from '@mui/material/Link';
import styled from "@mui/system/styled";

const StyledBox = styled(Box)`
    display: flex;
    justify-content: space-around;
    gap: 2em;
`;

const StyledItem = styled(Box)`
    text-align: center;
`;

const StyledSection = styled('section')`
    text-align:center;
    padding: 2em 2em;
`;

const Contact = () => {
    return (
        <StyledSection id="contact" title="Contact Me">
            <Box sx={{ width: '300px', marginBottom: '2em', marginX: 'auto' }}>
                <Typography fontSize="40px" fontWeight="700" variant="h2" gutterBottom>
                    Contact Me
                </Typography>
                {/* <Typography variant="body2" gutterBottom>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                </Typography> */}
            </Box>
            <StyledBox>
                <StyledItem>
                    <svg style={{ width: '40px' }} xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor">
                        <path d="M2.003 5.884L10 9.882l7.997-3.998A2 2 0 0016 4H4a2 2 0 00-1.997 1.884z" />
                        <path d="M18 8.118l-8 4-8-4V14a2 2 0 002 2h12a2 2 0 002-2V8.118z" />
                    </svg>
                    <Typography gutterBottom fontWeight="600" color="primary.main">
                        Email Me
                    </Typography>
                    <Typography variant="body2" color="secondary.main">
                        <Link title="VinoshenSilva@gmail.com" underline="none" href="mailto:VinoshenSilva@gmail.com">
                            VinoshenSilva@gmail.com
                        </Link>
                    </Typography>
                </StyledItem>
            </StyledBox>
        </StyledSection>
    )
}

export default Contact;