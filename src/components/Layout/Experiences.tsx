// Import material ui
import Typography from "@mui/material/Typography";
import styled from "@mui/system/styled";
import pink from "@mui/material/colors/pink";

// Import components
import ExperiencesListContainer from "@/components/Experience/ExperiencesListContainer";

const StyledSection = styled("section")(
  ({ theme }) => `
    text-align:center;
    padding: 6em 0em;
    color: white;
    background-color: #eb5067;
`
);

const Experiences = () => {
  return (
    <StyledSection id="experiences" title="Experiences">
      <Typography fontSize="40px" fontWeight="700" variant="h2" gutterBottom>
        Experiences
      </Typography>
      <ExperiencesListContainer />
    </StyledSection>
  );
};

export default Experiences;
