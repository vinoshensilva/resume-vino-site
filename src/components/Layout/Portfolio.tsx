// Import from libraries
import { useSelector } from "react-redux";
import { useEffect } from "react";

// Import from reducer
import { getSelectedPortfolio } from "@/reducer/portfolio";

// Import material ui
import Typography from '@mui/material/Typography';
import styled from "@mui/system/styled";
import { grey } from "@mui/material/colors";
import { Link } from "@mui/material";

// Import components
import ImageGallery from "@/components/UI/ImageGallery";

const StyledSection = styled('section')`
    text-align:center;
    padding: 6em 2em;

    display: flex;
    flex-direction: column;
    gap: 1em;
`;

const StyledFlexBox = styled('div')(({ theme }) => `
  display: grid;
  grid-template-columns: repeat(2, 50%);

  gap: 1em;

  @media (max-width: ${theme.breakpoints.values.lg}px){
    display: flex;
    flex-direction: column;
    gap: 1em;
  }
`)

const StyledDescription = styled('div')`
  padding: 0em;
  display: flex;
  flex-direction: column;
  gap: 0.75em;
  text-align: left;
`;

const PortfolioDescription = (description: string) => {
  if (!description) {
    return <></>;
  }

  return <div>
    <Typography variant="h6" fontWeight="600">
      Description
    </Typography>
    <Typography color="secondary.main" variant="body2">
      {description}
    </Typography>
  </div>
}

const PortfolioResult = (result: string) => {
  if (!result) {
    return <></>;
  }

  return <div>
    <Typography variant="h6" fontWeight="600">
      Results
    </Typography>
    <Typography color="secondary.main" variant="body2">
      {result}
    </Typography>
  </div>
}

const PortfolioNote = (note: string | undefined) => {
  if (!note) {
    return <></>;
  }

  return <div>
    <Typography variant="h6" fontWeight="600">
      Note
    </Typography>
    <Typography color="secondary.main" fontSize="12px" variant="subtitle2">
      {note}
    </Typography>
  </div>
}

const PortfolioLink = (link: string | undefined) => {
  if (!link) {
    return <></>;
  }

  return <div>
    <Typography variant="h6" fontWeight="600">
      Link
    </Typography>
    <Typography color="secondary.main" variant="subtitle2" fontWeight="600">
      Click <Link underline="none" href={link} target="_blank">here</Link> to see the project
    </Typography>
  </div>
}

const Portfolio = () => {
  const portfolio = useSelector(getSelectedPortfolio);

  useEffect(() => {
    if (portfolio) {
      document.querySelector("#portfolio")?.scrollIntoView({
        behavior: "smooth"
      })
    }
  }, [portfolio])

  if (!portfolio) {
    return <></>;
  }

  return (
    <StyledSection id="portfolio" title="Portfolio">
      <Typography color={grey[800]} fontWeight="700" variant="h4">
        {portfolio.name}
      </Typography>
      <StyledFlexBox>
        <div>
          <ImageGallery
            images={portfolio.images}
          />
        </div>
        <StyledDescription>
          {PortfolioLink(portfolio.link)}
          {PortfolioNote(portfolio.note)}
          {PortfolioDescription(portfolio.description)}
          {PortfolioResult(portfolio.result)}
        </StyledDescription>
      </StyledFlexBox>
    </StyledSection>
  );
}

export default Portfolio