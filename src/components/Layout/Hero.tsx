// Import material ui
import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import styled from "@mui/system/styled";

const StyledHeroSection = styled("section")(
  ({ theme }) => `
  height: 100vh;
  background-image: linear-gradient(rgba(0, 0, 0, 0.25), rgba(0, 0, 0, 0.075)), url("/images/hero-background.webp");
  background-size: cover;
  background-attachment: fixed;

  display: flex;
  flex-flow: row;
  align-items: center;

  @media (max-width: ${theme.breakpoints.values.md}px){
    background-position: 30%;
  }
`
);

const StyledBox = styled(Box)(
  ({ theme }) => `
    max-width: 41.6%;
    margin-left: auto;
    display: flex;
    gap: 1em;
    flex-direction: column;
    align-items: flex-start;
    
    padding-right: 40px;
    
    @media (max-width: ${theme.breakpoints.values.sm}px){
        padding-right: 10px;
        max-width: 55%;
    }
`
);

const StyledButton = styled(Button)`
  text-transform: none;
`;

const Hero = () => {
  return (
    <StyledHeroSection id="home">
      <StyledBox>
        <Typography
          fontWeight="800"
          color="secondary.main"
          fontSize="52px"
          variant="h1"
          sx={{
            fontSize: {
              xs: "25px",
              md: "52px",
            },
          }}
        >
          Hello my name is{" "}
          <Box component="span" color="primary.main">
            Vinoshen
          </Box>
        </Typography>

        {/* <Typography
                    color="white"
                >
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                </Typography> */}

        <a href="#about" style={{ textDecoration: "none" }}>
          <StyledButton variant="contained">About Me</StyledButton>
        </a>
      </StyledBox>
    </StyledHeroSection>
  );
};

export default Hero;
