// Import material ui
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';
import styled from "@mui/system/styled"
import grey from '@mui/material/colors/grey';

// Import components
import CategoriesContainer from '@/components/Category/CategoriesContainer';
import PortfolioContainer from '@/components/Portfolio/PortfolioContainer';

const StyledSection = styled('section')`
    text-align:center;
    padding: 6em 2em;

    display: flex;
    flex-direction: column;
    gap: 2em;
`;

const Portfolios = () => {
    return (
        <StyledSection id="portfolios" title="Portolios">
            <Box sx={{ maxWidth: '400px', marginBottom: '1em', marginX: 'auto' }}>
                <Typography fontSize="20px" gutterBottom fontWeight="700" variant="h2" >
                    Portfolios
                </Typography>
                <Typography
                    sx={
                        {
                            wordBreak: "break-word",
                            fontSize: {
                                xs: "30px", sm: "3rem"
                            }
                        }
                    }
                    color={grey[800]}
                    variant="h3"
                >
                    I Am Passionate About What I Do
                </Typography>
            </Box>
            <CategoriesContainer />
            <PortfolioContainer />
        </StyledSection >
    )
}

export default Portfolios;