import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";
import styled from "@mui/system/styled";

const StyledSection = styled("section")(
  ({ theme }) => `
    text-align:center;
    padding: 6em 2em;
    color: white;
    background-color: ${theme.palette.primary.main};
`
);

const About = () => {
  return (
    <StyledSection id="about" title="About Me">
      <Box sx={{ width: "300px", marginX: "auto" }}>
        <Typography
          fontSize="40px"
          fontWeight="700"
          variant="h2"
          gutterBottom
          color="white"
        >
          <Box color={"secondary.main"} component="span">
            About
          </Box>{" "}
          Me
        </Typography>
        <Box sx={{ display: "flex", gap: "1em", flexDirection: "column" }}>
          <Typography variant="body2" gutterBottom>
            Graduated with Bachelors of Computer Science from Multimedia
            University Cyberjaya .
          </Typography>
          {/* <Typography variant="body2">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                    </Typography> */}
        </Box>
      </Box>
    </StyledSection>
  );
};

export default About;
