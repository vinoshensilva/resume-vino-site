import Typography from '@mui/material/Typography';
import styled from "@mui/system/styled";
import technologies from '@/data/technologies';

const StyledSection = styled('section')(({ theme }) => `
    text-align:center;
    padding: 6em 2em;
    background-color: ${theme.palette.primary.main};
    color: white;
`);

const StyledDiv = styled('div')`
    display: flex;
    flex-wrap: wrap;
    justify-content: center;
    gap: 1em;
    padding: 2em;
`;

const Technologies = () => {
    return (
        <StyledSection id="technologies" title="Technologies">
            <Typography fontSize="40px" fontWeight="700" variant="h2" gutterBottom>
                Technologies
            </Typography>
            <StyledDiv>
                {
                    technologies.map((technology) => <div
                        key={technology.name}
                    >
                        <img height="40px" width="40px" src={technology.image} />
                        <p>{technology.name}</p>
                    </div>)
                }
            </StyledDiv>
        </StyledSection>
    )
}

export default Technologies;