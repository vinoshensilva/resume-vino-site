// Import libraries
import { useState } from "react";

// Import material ui
import Button from "@mui/material/Button";
import styled from "@mui/system/styled";

// Import scroll
import useScroll from "@/hooks/useScroll";
import { red } from "@mui/material/colors";

interface StyledButtonProps {
    show: boolean
}

const StyledButton = styled(Button) <StyledButtonProps>`
    position: fixed;
    right: 3vw;
    border-radius: 2em;
    min-width: 20px;
    padding: 1em;
    background-color: ${red[800]};

    -webkit-transition: all 0.5s ease-out;
    transition: all 0.5s ease-out;
    
    ${props => {
        if (props.show) {
            return `
                bottom: 2vh;   
            `;
        }
        else {
            return `
                bottom: -10vh;   
            `;
        }
    }
    }
`;

const ScrollTop = () => {

    const [show, setShow] = useState(false);

    const onScroll: EventListener = () => {
        if (window.pageYOffset > window.innerHeight / 4) {
            setShow(true);
        }
        else {
            setShow(false);
        }
    }

    const onClick = () => {
        window.scrollTo({
            top: 0,
            behavior: "smooth"
        })
    }

    useScroll(onScroll);

    return (
        <>
            <StyledButton show={show} color="secondary" size="small" variant="contained" onClick={onClick}>
                <svg xmlns="http://www.w3.org/2000/svg" width="20px" height="20px" viewBox="0 0 20 20" fill="currentColor">
                    <path fillRule="evenodd" d="M5.293 9.707a1 1 0 010-1.414l4-4a1 1 0 011.414 0l4 4a1 1 0 01-1.414 1.414L11 7.414V15a1 1 0 11-2 0V7.414L6.707 9.707a1 1 0 01-1.414 0z" clipRule="evenodd" />
                </svg>
            </StyledButton>
        </>
    )
}

export default ScrollTop