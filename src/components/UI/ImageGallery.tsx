// Import material ui
import styled from "@mui/system/styled";

const StyledDiv = styled("div")(
  ({ theme }) => `
    width: 100%;
    max-width: 600px;
    margin: auto;
    height: 350px;
    position: relative;
    overflow: hidden;

    @media (max-width: ${theme.breakpoints.values.sm}px){
        height: 200px;
    }
`
);

const StyledImageDiv = styled("div")<StyledImageProps>`
  width: 100%;
  height: 100%;
  position: absolute;
  transition: all 0.5s;
  pointer-events: none;

  ${(props) => {
    return `
            transform: translateX(${props.positionX}%)
        `;
  }}
`;

interface StyledImageProps {
  positionX: number;
}

const StyledImage = styled("img")`
  width: 100%;
  height: 100%;
  object-fit: contain;
  pointer-events: none;
  /* border: 1px solid ${grey[400]}; */
`;

const StyledButton = styled(ButtonBase)(
  ({ theme }) => `
    color: ${grey[800]};
    pointer-events: auto;
    &.active {
        color: ${theme.palette.primary.main};
    }
`
);

const StyledButtonsDiv = styled("div")`
  position: absolute;
  bottom: 2px;
  left: 0;
  right: 0;
  pointer-events: none;
`;

// Import hooks
import useSwipe, { SwipeDirection } from "@/hooks/useSwipe";

// Import libraries
import { useEffect, useRef, useState } from "react";

// Import functions
import { clamp } from "@/functions/number";
import { grey } from "@mui/material/colors";
import { ButtonBase, Typography } from "@mui/material";

type ImageGalleryProps = {
  images: string[];
};

const ImageGallery = ({ images }: ImageGalleryProps) => {
  const [ref, setRef] = useState<HTMLElement | null>(null);
  const [index, setIndex] = useState(0);

  const imagesRef = useRef<string[]>([]);

  // If the images were to be changed,
  // we want to start back from the first image
  useEffect(() => {
    setIndex(0);
    imagesRef.current = images;
  }, [images]);

  const onSwipeLeft = () => {
    setIndex((prevState) =>
      clamp(prevState - 1, 0, imagesRef.current.length - 1)
    );
  };

  const onSwipeRight = () => {
    setIndex((prevState) =>
      clamp(prevState + 1, 0, imagesRef.current.length - 1)
    );
  };

  const onSwipe = (direction: SwipeDirection) => {
    if (images.length === 0) {
      return;
    }

    switch (direction) {
      case SwipeDirection.Left:
        onSwipeRight();
        break;
      case SwipeDirection.Right:
        onSwipeLeft();
        break;
      default:
        break;
    }
  };

  const onClickPage = (newIndex: number) => {
    setIndex(clamp(newIndex, 0, images.length - 1));
  };

  useSwipe(ref || null, onSwipe);

  if (images.length === 0) {
    return <></>;
  }

  return (
    <StyledDiv
      ref={(el) => {
        setRef(el);
      }}
    >
      {images.map((image, i) => {
        return (
          <StyledImageDiv positionX={(i - index) * 100} key={image}>
            <StyledImage src={image} />
          </StyledImageDiv>
        );
      })}
      <StyledButtonsDiv>
        {images.map((image, i) => {
          return (
            <StyledButton
              key={image}
              onClick={onClickPage.bind(null, i)}
              className={i === index ? "active" : ""}
            >
              <Typography fontSize="50px" fontWeight="600">
                .
              </Typography>
            </StyledButton>
          );
        })}
      </StyledButtonsDiv>
    </StyledDiv>
  );
};

export default ImageGallery;
