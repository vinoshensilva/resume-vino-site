// Import type
import ExperienceType from "@/types/experience";

// Import material ui
import styled from "@mui/system/styled"

// Import component
import ExperienceCard from "@/components/Experience/ExperienceCard";

const StyledDiv = styled('div')(({ theme }) => `
  display: grid;
  gap: 2em;
  padding: 2em;
  
  grid-template-columns: repeat(3,auto);
  justify-content: space-around;

  @media (max-width: ${theme.breakpoints.values.lg}px){
      grid-template-columns: repeat(1,auto);
  }

  @media (max-width: ${theme.breakpoints.values.md}px){
      grid-template-columns: repeat(1,auto);
  }
`);

const ExperiencesList = ({ experiences }: { experiences: ExperienceType[] }) => {
    return (
        <StyledDiv>
            {experiences.map((experience) => {
                return (
                    <ExperienceCard
                        key={experience.id}
                        company={experience.company}
                        description={experience.description}
                        image={experience.image}
                        position={experience.position}
                        timeline={experience.timeline}
                    />
                )
            })}
        </StyledDiv>
    )
}

export default ExperiencesList