// Import data
import experiences from "@/data/experiences";

// Import component
import ExperiencesList from "@/components/Experience/ExperiencesList";

const ExperiencesListContainer = () => {
    return (
        <ExperiencesList experiences={experiences} />
    );
}

export default ExperiencesListContainer