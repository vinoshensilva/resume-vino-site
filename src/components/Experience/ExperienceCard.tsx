// Import material ui
import Typography from "@mui/material/Typography";
import styled from "@mui/system/styled"

// Import types
type ExperienceProps = {
  image: string;
  description: string;
  company: string;
  position: string;
  timeline: string;
}

const StyledDiv = styled('div')`
  display: flex;
  flex-direction: column;
  gap: 0.5em;
  padding: 1em;
  width: 300px;
`;

const StyledImg = styled('img')`
  width: 60px;
  height: 60px;
  border-radius: 2em;
  margin: 0 auto;
`;

const Experience = ({ image, description, company, position, timeline }: ExperienceProps) => {
  return (
    <StyledDiv>
      <StyledImg src={image} />
      <Typography fontWeight="700" variant="h6">
        {company}
      </Typography>
      <Typography fontWeight="600" variant="body1">
        {position}
      </Typography>
      <Typography variant="caption">
        {timeline}
      </Typography>
      {/* <Typography lineHeight="1.5em" variant="subtitle2">
        {description}
      </Typography> */}
    </StyledDiv>
  )
}

export default Experience