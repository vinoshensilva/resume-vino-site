// Import material ui 
import styled from "@mui/system/styled"
import Button from "@mui/material/Button";
import { grey } from "@mui/material/colors";

const ActiveCategoryButton = styled(Button)`
    border-radius: 2em;
`

const InActiveCategoryButton = styled(Button)`
    border-radius: 2em;

    color: ${grey[600]};

    transition: color 0.5s;

    &:hover {
        color: black;
        background-color: transparent;
    }
`;

type CategoryProps = {
    category: string,
    onClick: () => void,
    selected: boolean
}

const Category = ({ category, onClick, selected }: CategoryProps) => {
    return (
        selected ?
            <ActiveCategoryButton size="small" variant="contained" onClick={onClick} >
                {category}
            </ActiveCategoryButton>
            : <InActiveCategoryButton size="small" onClick={onClick}>
                {category}
            </InActiveCategoryButton>
    )
}

export default Category