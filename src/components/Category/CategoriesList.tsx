// Import material ui 
import styled from "@mui/system/styled"
import Box from "@mui/material/Box";

// Import components
import Category from "@/components/Category/Category";

type CategoriesListProps = {
    categories: string[],
    currentCategory: string,
    onCategoryClick: (category: string) => void
}

const StyledBox = styled(Box)`
    display: flex;
    gap: 1em;
    justify-content: center;
    flex-wrap: wrap;
`;

const CategoriesList = ({ currentCategory, categories, onCategoryClick }: CategoriesListProps) => {
    return (
        <StyledBox>
            {
                categories.map((category) => {
                    return (
                        <Category
                            key={category}
                            onClick={onCategoryClick.bind(null, category)}
                            category={category}
                            selected={category === currentCategory}
                        />
                    );
                })
            }
        </StyledBox>
    )
}

export default CategoriesList