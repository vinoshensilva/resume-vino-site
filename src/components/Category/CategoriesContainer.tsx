// Import libraries
import { useSelector } from "react-redux";

// Import from reducer and store
import { changeCategory, selectAllCategories, getCategory } from "@/reducer/portfolio";
import { useAppDispatch } from "@/store";

// Import components
import CategoriesList from "@/components/Category/CategoriesList";

const CategoriesContainer = () => {

    const categories = useSelector(selectAllCategories);

    const category = useSelector(getCategory);

    const dispatch = useAppDispatch();

    const categoryClickHandler = (newCategory: string) => {
        dispatch(changeCategory({ newCategory }));
    }

    return (
        <>
            <CategoriesList
                currentCategory={category}
                categories={categories}
                onCategoryClick={categoryClickHandler}
            />
        </>
    )
}

export default CategoriesContainer