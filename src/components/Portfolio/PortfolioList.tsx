// Import material ui
import { styled } from "@mui/system";

// Import types
import PortFolioType from "@/types/portfolio";
import PortfolioCard from "./PortfolioCard";

type PortfolioListProps = {
    portfolios: PortFolioType[],
    onPortfolioClick: (portfolioId: string) => void
};

const StyledDiv = styled('div')(({ theme }) => `
    display: grid;
    grid-template-columns: repeat(2,auto);
    justify-content: center;
    gap: 1em;

    @media (max-width: ${theme.breakpoints.values.lg}px) {
        grid-template-columns: repeat(2,300px);
    }
    
    @media (max-width: ${theme.breakpoints.values.sm}px) {
        grid-template-columns: repeat(1,250px);
    }
`);

const PortfolioList = ({ portfolios, onPortfolioClick }: PortfolioListProps) => {
    return (
        <StyledDiv>
            {
                portfolios.map((portfolio) => <PortfolioCard
                    key={portfolio.id}
                    name={portfolio.name}
                    thumbnail={portfolio.thumbnail}
                    onClick={onPortfolioClick.bind(null, portfolio.id)}
                />)
            }
        </StyledDiv>
    )
}

export default PortfolioList