// Import libraries
import { ButtonBase } from "@mui/material";
import { grey } from "@mui/material/colors";
import styled from "@mui/system/styled";

// Define type
type PortfolioCardProps = {
    name: string;
    thumbnail: string;
    onClick: () => void
};

const StyledButton = styled(ButtonBase)(({ theme }) => `
    border: 1px solid ${grey[300]};
    background-color: ${theme.palette.primary.main};
    position: relative;

    &:hover p {
        opacity: 1;
    }
`);

const StyledImg = styled('img')`
    width: 100%;
    height: 100%;

    transition: opacity 0.15s ease-in;
    
    &:hover {
        opacity: 0.25;
    }
`;

const StyledText = styled('p')`
    position: absolute;
    font-size: 3.2vw;
    top: 1/2;
    left: 1/2;
    color: white;
    opacity: 0;
`;

const PortfolioCard = ({ name, thumbnail, onClick }: PortfolioCardProps) => {
    return (
        <StyledButton onClick={onClick}>
            <StyledText id="dfsd">
                {name}
            </StyledText>
            <StyledImg src={thumbnail} />
        </StyledButton>
    )
}

export default PortfolioCard