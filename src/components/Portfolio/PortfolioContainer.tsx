// Import libraries
import { useSelector } from "react-redux";

// Import components
import PortfolioList from "@/components/Portfolio/PortfolioList";

// Import from reducer
import { selectPortfoliosByCategory, showProject } from "@/reducer/portfolio";
import { useAppDispatch } from "@/store";

const PortfolioContainer = () => {
  const portfolios = useSelector(selectPortfoliosByCategory);

  const dispatch = useAppDispatch();

  const portfolioClickHandler = (portfolioId: string) => {
    dispatch(showProject({ portfolioId }));
  };

  return (
    <PortfolioList
      portfolios={portfolios}
      onPortfolioClick={portfolioClickHandler}
    />
  );
};

export default PortfolioContainer;
