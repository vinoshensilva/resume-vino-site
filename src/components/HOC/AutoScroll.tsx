import { useEffect } from "react"

const AutoScroll = ({ children }: { children: JSX.Element[] | JSX.Element | undefined }) => {
    useEffect(() => {
        if (window.location.hash) {
            document.querySelector(window.location.hash)?.scrollIntoView({
                behavior: "smooth"
            })
        }
    }, []);

    return (
        <>{children}</>
    )
}

export default AutoScroll