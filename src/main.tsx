import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './App'

import { ThemeProvider } from '@mui/material/styles'
import CssBaseline from "@mui/material/CssBaseline";

import themeOptions from '@/theme';

import './index.css';

// Import library
import { Provider } from 'react-redux';

// Import store
import { store } from '@/store';

ReactDOM.createRoot(document.getElementById('root') as HTMLElement).render(
  <React.StrictMode>
    <Provider store={store} >
      <ThemeProvider theme={themeOptions}>
        <CssBaseline />
        <App />
      </ThemeProvider>
    </Provider>
  </React.StrictMode>
)
